# Master Trainer Program (MTP) on "Unmanned Aircraft Systems - Allied Technologies"  @ IIITH

Master Trainer Program (MTP) is a capacity building for human resource development in Unmanned Aircraft Systems. This is a week-long course conducted twice a year by Robotics Research Center, IIIT Hyderabad
 
**Allied UAS Technologies**: Flight mechanics, Flight dynamics, Planning and navigation, Cybersecurity for internet of drones, IoT for drones, RTOS for drones, Wireless communication, Remote sensing, image processing and a few other topics related to Unmanned Aircraft System.

**Drone Programming**: ArduPilot, PyMAVLink, ROS and Gazebo, PyBullet, Navigation-Guidance and Control for UAV, Image Processing and Security.

---

A repository to store all items pertaining to MTP `Unmanned Aircraft Systems - Allied Technologies`

## Table of contents

- [Master Trainer Program (MTP) on "Unmanned Aircraft Systems - Allied Technologies" @ IIITH](#mtp-unmanned-aircraft-systems-allied-technologies--iiith)
    - [Table of contents](#table-of-contents)
    - [Contents](#contents)
        - [Sessions and Resources](#sessions-and-resources)
    

## Contents

### Sessions and Resources

Sessions and Resources of the MTP are as follows

| Session   |   Speaker | Resources   |
|-----------|:---------------------| -------------------:|
| Introduction to UAV | [Harikumar Kandath](https://sites.google.com/view/harikumar-kandath/home) | [View](./Lecture-Slides/MTP-IntroductiontoUAV.pdf)|
| Kinematics and Dynamics | [Nagamanikandan Govindan](https://nagamanigi.wixsite.com/home) | [View](./Lecture-Slides/UAS_Kinematics and Dynamics.pdf)|
| Image Processing | [Ravi Kiran Sarvadevabhatla](url) | [View](./Lecture-Slides/MTP-ImageProcessing.pdf)|
| RTOS for Drones - I | [Deepak Gangadhran](https://gdeepak11.github.io/) | [View](./Lecture-Slides/RTOS_for_DronesI.pptx)|
| RTOS for Drones - II | [Deepak Gangadhran](https://gdeepak11.github.io/) | [View](./Lecture-Slides/RTOS_for_DronesII.pptx)|
| UAV Communication for 6G and Beyond | [Kali Krishna Kota](https://www.linkedin.com/in/kali-krishna-kota-589636105/) | [View](./Lecture-Slides/MTP-UAVCommunication.pdf)|
| IOT: Introduction, Applications and Research Activities | [Sachin Chaudhari](https://faculty.iiit.ac.in/~sachin.c/) | [View](./Lecture-Slides/MTP-IoT.pdf)|
| Earth Observation Platforms | [Rama Chandra Prasad](https://www.iiit.ac.in/people/faculty/pramachandra/) | [View](./Lecture-Slides/RemoteSensing-I.pdf)|
| Earth Observation Sensors - Data Acquisition and Processing | [Rama Chandra Prasad](https://www.iiit.ac.in/people/faculty/pramachandra/) | [View](./Lecture-Slides/RemoteSensing-II.pdf)|
| UAV: Guidance, Navigation and Control | [Harikumar Kandath](https://sites.google.com/view/harikumar-kandath/home) | [View](./Lecture-Slides/UAV-NGC.pdf)|
| Trajectory Planning | [Nagamanikandan Govindan](https://nagamanigi.wixsite.com/home) | [View](./Lecture-Slides/Trajectory Generation.pdf)|
| Ardupilot |  | [View](./Lecture-Slides/Ardupilot.pdf)|
| UAV Simulation using PyBullet |  | [View](./Lecture-Slides/Pybullet.pdf)|
| Microsoft AirSim              |  | [View](./Lecture-Slides/Microsoft_AirSim.pdf)
| Computer Vision | [Bitla Bhanu Teja](url) | [View](./Lecture-Slides/Computer Vision.pptx)|
| Software Engineering: A birds eye view of the why and how! | [Karthik Vaidhyanthan](https://karthikvaidhyanathan.com/)  | [View](./Lecture-Slides/Software-Engineering.pptx)|
| Introduction to UAV Security | [Suhas V](url) | [View](./Lecture-Slides/UAV Security.pptx)|




















